package currencyexchanger.ais.com.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import currencyexchanger.ais.com.CurrencyListAdapter;
import currencyexchanger.ais.com.R;
import currencyexchanger.ais.com.model.Currency;
import currencyexchanger.ais.com.model.CurrencyModel;
import currencyexchanger.ais.com.model.CurrencyRate;
import currencyexchanger.ais.com.model.CurrencyRateDeserializer;
import currencyexchanger.ais.com.model.CurrencyResponse;
import currencyexchanger.ais.com.model.CurrencyViewModelResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyViewModel extends ViewModel implements CurrencyListAdapter.OnCurrencyChangedListener {

    private CurrencyModel currencyModel;
    private MutableLiveData<CurrencyViewModelResponse> currencyRates;
    private Currency currentCurrency = Currency.EUR;
    private Double currentKoef = 0.0d;

    CurrencyViewModel(CurrencyModel currencyModel) {
        this.currencyModel = currencyModel;
    }

    public LiveData<CurrencyViewModelResponse> getCurrencyRates() {
        currencyRates = new MutableLiveData<>();
        loadCurrencies(currentCurrency);
        return currencyRates;
    }

    private void loadCurrencies(final Currency newCurrency) {
        this.currentCurrency = newCurrency;
        currencyModel.getCurrencies(newCurrency.getText()).enqueue(new Callback<CurrencyResponse>() {
            @Override
            public void onResponse(@NonNull Call<CurrencyResponse> call, @NonNull Response<CurrencyResponse> response) {
                List<CurrencyRate> currencyRatesResponse = response.body().rates;
                for (int i = 0; i < currencyRatesResponse.size(); i++) {
                    CurrencyRate currentCurrencyRate = currencyRatesResponse.get(i);
                    currentCurrencyRate.value = currentKoef * currentCurrencyRate.rate;
                }
                CurrencyRate mainCurrency = CurrencyRateDeserializer.getByShortName(currentCurrency, 1.0d);
                mainCurrency.value = currentKoef;
                currencyRatesResponse.add(0, mainCurrency);
                currencyRates.setValue(new CurrencyViewModelResponse(currencyRatesResponse, null));
            }

            @Override
            public void onFailure(@NonNull Call<CurrencyResponse> call, @NonNull Throwable error) {
                CurrencyViewModelResponse currencyViewModelResponse = currencyRates.getValue();
                List<CurrencyRate> currencyRatesValue = currencyViewModelResponse == null ? null : currencyViewModelResponse.currencyRates;
                currencyRates.setValue(new CurrencyViewModelResponse(currencyRatesValue, R.string.something_went_wrong));
            }
        });

    }

    @Override
    public void onCurrencyChange(CharSequence newValue, int position) {
        CurrencyViewModelResponse currencyViewModelResponse = this.currencyRates.getValue();
        currentKoef = Double.parseDouble(newValue.toString().replaceAll(",", "."));

        if (currencyViewModelResponse != null) {
            List<CurrencyRate> currencyRates = currencyViewModelResponse.currencyRates;
            if (position != 0) {
                loadCurrencies(currencyRates.get(position).shortName);
                return;
            }
            for (int i = 0; i < currencyRates.size(); i++) {
                if (i != position) {
                    currencyRates.get(i).value = currentKoef * currencyRates.get(i).rate;
                } else {
                    currencyRates.get(i).value = currentKoef;
                }
            }
            this.currencyRates.setValue(new CurrencyViewModelResponse(currencyRates, null));
        }
    }

    public static class Factory implements ViewModelProvider.Factory {
        private final CurrencyModel currencyModel;

        public Factory(CurrencyModel currencyModel) {
            this.currencyModel = currencyModel;
        }

        @NonNull
        @SuppressWarnings("unchecked")
        @Override
        public CurrencyViewModel create(@NonNull Class modelClass) {
            return new CurrencyViewModel(currencyModel);
        }
    }
}