package currencyexchanger.ais.com;

import android.app.Application;

import currencyexchanger.ais.com.dagger.AppComponent;
import currencyexchanger.ais.com.dagger.DaggerAppComponent;

public class App extends Application {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().build();
        appComponent.inject(this);
    }

}