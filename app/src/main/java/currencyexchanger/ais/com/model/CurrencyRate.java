package currencyexchanger.ais.com.model;

public class CurrencyRate {
    public Currency shortName;
    public int fullName;
    public Double rate;
    public int drawableId;
    public double value;

    public CurrencyRate(Currency shortName, int fullName, Double rate, int drawableId) {
        this.shortName = shortName;
        this.fullName = fullName;
        this.rate = rate;
        this.drawableId = drawableId;
    }

    @Override
    public String toString() {
        return "CurrencyRate{" +
                "shortName='" + shortName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", rate=" + rate +
                ", drawableId=" + drawableId +
                '}';
    }
}
