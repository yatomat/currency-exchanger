package currencyexchanger.ais.com.model;

public enum Currency {
    EUR("EUR"),
    BGN("BGN"),
    BRL("BRL"),
    AUD("AUD"),
    CAD("CAD"),
    CHF("CHF"),
    CNY("CNY"),
    CZK("CZK"),
    DKK("DKK"),
    GBP("GBP"),
    HRK("HRK"),
    HUF("HUF"),
    IDR("IDR"),
    ILS("ILS"),
    INR("INR"),
    ISK("ISK"),
    JPY("JPY"),
    RUB("RUB"),
    USD("USD");

    private String shortName;

    Currency(String shortName) {
        this.shortName = shortName;
    }

    public String getText() {
        return this.shortName;
    }

    public static Currency fromString(String text) {
        for (Currency currency : Currency.values()) {
            if (currency.shortName.equalsIgnoreCase(text)) {
                return currency;
            }
        }
        return null;
    }
    }
