package currencyexchanger.ais.com.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import currencyexchanger.ais.com.R;
import currencyexchanger.ais.com.model.Currency;
import currencyexchanger.ais.com.model.CurrencyRate;


public class CurrencyRateDeserializer implements JsonDeserializer<List<CurrencyRate>> {
    @Override
    public List<CurrencyRate> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Set<Map.Entry<String, JsonElement>> entries = json.getAsJsonObject().entrySet();
        Iterator<Map.Entry<String, JsonElement>> iterator = entries.iterator();
        List<CurrencyRate> currencyRates = new ArrayList<>();
        while (iterator.hasNext()) {
            Map.Entry<String, JsonElement> setElement = iterator.next();
            Currency key = Currency.fromString(setElement.getKey());
            if (key != null) {
                CurrencyRate currencyRate = getByShortName(key, setElement.getValue().getAsDouble());
                if (currencyRate != null)
                    currencyRates.add(currencyRate);
            }
        }
        return currencyRates;
    }

    public static CurrencyRate getByShortName(Currency shortName, Double coefficient) {
        switch (shortName) {
            case AUD:
                return new CurrencyRate(shortName, R.string.curr_aud, coefficient, R.drawable.australia);

            case BGN:
                return new CurrencyRate(shortName, R.string.curr_bgn, coefficient, R.drawable.bulgaria_flag_icon_32);

            case BRL:
                return new CurrencyRate(shortName, R.string.curr_brl, coefficient, R.drawable.brazil);

            case CAD:
                return new CurrencyRate(shortName, R.string.curr_cad, coefficient, R.drawable.canada_flag_icon_32);

            case CHF:
                return new CurrencyRate(shortName, R.string.curr_chf, coefficient, R.drawable.switzerland_flag_icon_32);

            case CNY:
                return new CurrencyRate(shortName, R.string.curr_cny, coefficient, R.drawable.china_flag_icon_32);

            case CZK:
                return new CurrencyRate(shortName, R.string.curr_czk, coefficient, R.drawable.czech_republic_flag_icon_32);

            case DKK:
                return new CurrencyRate(shortName, R.string.curr_dkk, coefficient, R.drawable.denmark_flag_icon_32);

            case GBP:
                return new CurrencyRate(shortName, R.string.curr_gbp, coefficient, R.drawable.united_kingdom_flag_icon_32);

            case HRK:
                return new CurrencyRate(shortName, R.string.curr_hrk, coefficient, R.drawable.croatia_flag_icon_32);

            case HUF:
                return new CurrencyRate(shortName, R.string.curr_huf, coefficient, R.drawable.hungary_flag_icon_32);

            case IDR:
                return new CurrencyRate(shortName, R.string.curr_idr, coefficient, R.drawable.indonesia_flag_icon_32);

            case ILS:
                return new CurrencyRate(shortName, R.string.curr_ils, coefficient, R.drawable.israel_flag_icon_32);

            case INR:
                return new CurrencyRate(shortName, R.string.curr_inr, coefficient, R.drawable.india_flag_icon_32);


            case ISK:
                return new CurrencyRate(shortName, R.string.curr_isk, coefficient, R.drawable.iceland_flag_icon_32);


            case JPY:
                return new CurrencyRate(shortName, R.string.curr_jpy, coefficient, R.drawable.japan_flag_icon_32);

            case RUB:
                return new CurrencyRate(shortName, R.string.curr_rus, coefficient, R.drawable.russia_flag_icon);

            case USD:
                return new CurrencyRate(shortName, R.string.curr_usd, coefficient, R.drawable.united_states_of_america_flag_icon_32);

            case EUR:
                return new CurrencyRate(shortName, R.string.curr_eur, coefficient, R.drawable.european_union_flag_icon);
            default:
                return null;

        }
    }

    ;
}
