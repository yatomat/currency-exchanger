package currencyexchanger.ais.com.model;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import java.util.List;

public class CurrencyViewModelResponse {
    public List<CurrencyRate> currencyRates;
    @Nullable
    @StringRes
    public Integer errorMessage;

    public CurrencyViewModelResponse(List<CurrencyRate> currencyRates, @Nullable Integer errorMessage) {
        this.currencyRates = currencyRates;
        this.errorMessage = errorMessage;
    }
}
