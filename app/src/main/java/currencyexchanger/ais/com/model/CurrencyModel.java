package currencyexchanger.ais.com.model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyModel {

    @GET("latest")
    Call<CurrencyResponse> getCurrencies(@Query("base") String base);
}
