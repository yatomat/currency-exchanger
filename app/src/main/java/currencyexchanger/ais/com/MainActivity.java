package currencyexchanger.ais.com;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.ProgressBar;

import javax.inject.Inject;

import currencyexchanger.ais.com.dagger.DaggerCurrencyComponent;
import currencyexchanger.ais.com.model.CurrencyViewModelResponse;
import currencyexchanger.ais.com.viewmodel.CurrencyViewModel;

public class MainActivity extends AppCompatActivity implements Observer<CurrencyViewModelResponse>, CurrencyListAdapter.OnCurrencyChangedListener {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    final static int refreshDelay = 1000;

    private RecyclerView recyclerView;
    private CurrencyListAdapter adapter;
    private ProgressBar pbLoading;
    private Handler checkingApiHandler;
    private CurrencyViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerCurrencyComponent.builder().appComponent(App.getAppComponent()).build().inject(this);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rv_currencies);
        pbLoading = findViewById(R.id.pb_loading);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        model = ViewModelProviders.of(this, viewModelFactory).get(CurrencyViewModel.class);

        adapter = new CurrencyListAdapter(this);
        recyclerView.setAdapter(adapter);
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }


    @Override
    protected void onStart() {
        super.onStart();
        checkingApiHandler = new Handler();
        initHandler();
        model.getCurrencyRates().observe(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        checkingApiHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onChanged(@Nullable CurrencyViewModelResponse currencyRatesResponse) {
        if (currencyRatesResponse != null) {
            if (pbLoading.getVisibility() != View.GONE) {
                pbLoading.setVisibility(View.GONE);
            }
            if (currencyRatesResponse.errorMessage == null) {
                adapter.updateCurrencyRates(currencyRatesResponse.currencyRates);
                adapter.notifyItemRangeChanged(1, currencyRatesResponse.currencyRates.size() - 1);
            } else {
                handleError(currencyRatesResponse.errorMessage);
            }
        }
    }

    private void handleError(@StringRes Integer errorMessage) {
        checkingApiHandler.removeCallbacksAndMessages(null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(errorMessage).setPositiveButton(R.string.retry_action, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pbLoading.setVisibility(View.VISIBLE);
                initHandler();
            }
        })
                .setTitle(R.string.error_dialog_title).setCancelable(false).create().show();
    }

    @Override
    public void onCurrencyChange(CharSequence newValue, int position) {
        model.onCurrencyChange(newValue, position);
        if (position != 0) {
            recyclerView.scrollToPosition(0);
        }
    }

    private void initHandler() {
        if (checkingApiHandler != null) {
            checkingApiHandler.postDelayed(new Runnable() {
                public void run() {
                    model.getCurrencyRates().observe(MainActivity.this, MainActivity.this);
                    checkingApiHandler.postDelayed(this, refreshDelay);
                }
            }, refreshDelay);
        }
    }
}
