package currencyexchanger.ais.com.dagger;

import android.app.Application;

import dagger.Component;
import retrofit2.Retrofit;

@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(Application application);

    Retrofit retrofit();
}
