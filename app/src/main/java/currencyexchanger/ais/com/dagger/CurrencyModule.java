package currencyexchanger.ais.com.dagger;

import android.arch.lifecycle.ViewModelProvider;

import currencyexchanger.ais.com.viewmodel.CurrencyViewModel;
import currencyexchanger.ais.com.model.CurrencyModel;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class CurrencyModule {

    @Provides
    CurrencyModel provideCurrencyModel(Retrofit retrofit) {
        return retrofit.create(CurrencyModel.class);
    }

    @Provides
    ViewModelProvider.Factory providesViewModelFactory(CurrencyModel currencyModel) {
        return new CurrencyViewModel.Factory(currencyModel);
    }
}
