package currencyexchanger.ais.com.dagger;

import currencyexchanger.ais.com.MainActivity;
import dagger.Component;

@Component(modules = {CurrencyModule.class}, dependencies = {AppComponent.class})
public interface CurrencyComponent {
    void inject(MainActivity activity);
}
