package currencyexchanger.ais.com.dagger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import currencyexchanger.ais.com.model.CurrencyRateDeserializer;
import currencyexchanger.ais.com.model.CurrencyRate;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {
    private static final String URL = "https://revolut.duckdns.org";

    @Provides
    Gson provideCustomDeserializer() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        JsonDeserializer<List<CurrencyRate>> deserializer = new CurrencyRateDeserializer();
        Type currencyRateListType = new TypeToken<List<CurrencyRate>>() {
        }.getType();
        gsonBuilder.registerTypeAdapter(currencyRateListType, deserializer);

        return gsonBuilder.create();
    }

    @Provides
    Retrofit provideRetrofit(Gson customDeserializer) {
        return new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(customDeserializer))
                .build();
    }
}
