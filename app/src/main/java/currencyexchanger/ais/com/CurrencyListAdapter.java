package currencyexchanger.ais.com;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import currencyexchanger.ais.com.model.CurrencyRate;
import de.hdodenhof.circleimageview.CircleImageView;

public class CurrencyListAdapter extends RecyclerView.Adapter<CurrencyListAdapter.ViewHolder> {

    private List<CurrencyRate> currencyRateList;
    private final OnCurrencyChangedListener onCurrencyChangedListener;
    private int positionChosenEditText;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvShortCurrencyName;
        public TextView tvFullCurrencyName;
        public CircleImageView civNationalFlag;
        public EditText etCurrencyValue;
        private TextWatcher textWatcher;

        public ViewHolder(View v) {
            super(v);
            tvShortCurrencyName = v.findViewById(R.id.tv_short_currency_name);
            etCurrencyValue = v.findViewById(R.id.et_currency_value);
            tvFullCurrencyName = v.findViewById(R.id.tv_currency);
            civNationalFlag = v.findViewById(R.id.civ_national_flag);
        }

        public void addTextWatcher(final OnCurrencyChangedListener listener, final OnFirstItemChangedListener onFirstItemChangedListener) {
            this.textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    listener.onCurrencyChange(charSequence, getAdapterPosition());
                    if (getAdapterPosition() != 0) {
                        onFirstItemChangedListener.onFirstItemChanged(getAdapterPosition());
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            etCurrencyValue.addTextChangedListener(textWatcher);
        }

        public void removeTextWatcher() {
            etCurrencyValue.removeTextChangedListener(textWatcher);
        }

        void addFocusListener(final OnItemSelectedListener onItemSelectedListener) {
            etCurrencyValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        onItemSelectedListener.onItemSelected(getAdapterPosition());
                    }
                }
            });
        }

        public interface OnFirstItemChangedListener {
            void onFirstItemChanged(int position);
        }

        public interface OnItemSelectedListener {
            void onItemSelected(int position);
        }
    }

    public CurrencyListAdapter(OnCurrencyChangedListener listener) {
        this.onCurrencyChangedListener = listener;
    }

    @NonNull
    @Override
    public CurrencyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                             int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CurrencyRate currencyRate = currencyRateList.get(position);
        holder.tvShortCurrencyName.setText(currencyRate.shortName.getText());
        holder.removeTextWatcher();
        if (positionChosenEditText != position || position == 0) {
            holder.etCurrencyValue.setText(String.format(Locale.getDefault(), "%.2f", currencyRate.value));
        }
        holder.civNationalFlag.setImageResource(currencyRate.drawableId);
        holder.tvFullCurrencyName.setText(currencyRate.fullName);
        holder.addFocusListener(new ViewHolder.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position) {
                CurrencyListAdapter.this.positionChosenEditText = position;
            }
        });
        holder.addTextWatcher(onCurrencyChangedListener, new ViewHolder.OnFirstItemChangedListener() {
            @Override
            public void onFirstItemChanged(int position) {
                CurrencyListAdapter.this.notifyItemMoved(position, 0);
                CurrencyListAdapter.this.positionChosenEditText = 0;
            }
        });
    }

    @Override
    public int getItemCount() {
        return currencyRateList == null ? 0 : currencyRateList.size();
    }

    public void updateCurrencyRates(List<CurrencyRate> currencyRates) {
        this.currencyRateList = currencyRates;
    }

    public interface OnCurrencyChangedListener {
        void onCurrencyChange(CharSequence newValue, int position);
    }

}
